import 'package:flutter/material.dart';

class RecipeDetail extends StatelessWidget {
  RecipeDetail({Key? key, required this.data}) : super(key: key);

  final Map data;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double width = size.width;
    final double height = size.height;

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Recipe Detail",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: width / 10),
            child: Container(
              alignment: Alignment.bottomLeft,
              padding: EdgeInsets.only(bottom: 20.0),
              child: Text(
                data['title'],
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            width: width,
            height: height / 3,
            decoration: BoxDecoration(
              image: DecorationImage(
                colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.25),
                  BlendMode.srcOver,
                ),
                fit: BoxFit.cover,
                image: NetworkImage(
                  data['image'],
                ),
              ),
            ),
          ),
          Container(
            color: Color(0xFFCAB7A1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                data['servings'] == 1
                    ? Text('1 person')
                    : Text(data['servings'].toString() + ' persons'),
                const SizedBox(
                  width: 10.0,
                ),
                const Text('|'),
                const SizedBox(
                  width: 10.0,
                ),
                Text(data['readyInMinutes'].toString() + ' minutes'),
              ],
            ),
            height: height / 10,
          ),
          Container(
            padding: EdgeInsets.only(left: width / 20, top: height / 40),
            child: const Text(
              "Ingredients",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: width / 10, top: height / 40, right: width / 10),
            child: Text(
              data['ingredients'],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: width / 20, top: height / 40),
            child: const Text(
              "Instructions",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: width / 10, top: height / 40, right: width / 10),
            child: Text(
              data['instructions'],
            ),
          ),
        ],
      ),
    );
  }
}
