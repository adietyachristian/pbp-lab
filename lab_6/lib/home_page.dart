import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lab_6/recipe_detail.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class Recipe {
  final int id;
  final String title;
  final String image;

  Recipe(this.id, this.title, this.image);
}

class _HomePageState extends State<HomePage> {
  int _resultFlex = 0;
  List<Recipe> _recipes = [];
  String _loading = "";

  void _getRecipe(String keyword) async {
    final http.Response response = await http.get(Uri.parse(
        'https://api.spoonacular.com/recipes/complexSearch?query=${keyword}&apiKey=093c3f2d670c4f29937ce2bf0b69b565'));

    if (response.statusCode == 200) {
      final dynamic responseJson = jsonDecode(response.body);

      if (responseJson['results'].length == 0) {
        _loading = "No result.";
        setState(() {});
        return;
      }

      for (var item in responseJson['results']) {
        _recipes.add(Recipe(
            item['id'], item['title'].toString(), item['image'].toString()));
      }
      setState(() {});
    }
  }

  Future<Map> _getRandomRecipe() async {
    final http.Response response = await http.get(Uri.parse(
        'https://api.spoonacular.com/recipes/random?apiKey=f00a4b4fb964493fb2cd468db364a7d3'));

    if (response.statusCode == 200) {
      Map responseJson = jsonDecode(response.body) as Map;
      String ingredients = '';
      String instructions = '';

      responseJson = responseJson['recipes'][0];

      for (var item in responseJson['extendedIngredients']) {
        ingredients += item['amount'].toString() +
            ' ' +
            item['unit'].toString() +
            ' ' +
            item['name'].toString() +
            '\n';
      }

      for (var item in responseJson['analyzedInstructions'][0]['steps']) {
        instructions +=
            item['number'].toString() + '. ' + item['step'].toString() + '\n';
      }

      responseJson['ingredients'] = ingredients;
      responseJson['instructions'] = instructions;

      return responseJson;
    }
    return {};
  }

  Future<Map> _getRecipeDetail(int id) async {
    final http.Response response = await http.get(Uri.parse(
        "https://api.spoonacular.com/recipes/" +
            id.toString() +
            "/information?apiKey=f00a4b4fb964493fb2cd468db364a7d3"));

    if (response.statusCode == 200) {
      final Map responseJson = jsonDecode(response.body) as Map;
      String ingredients = '';
      String instructions = '';

      for (var item in responseJson['extendedIngredients']) {
        ingredients += item['amount'].toString() +
            ' ' +
            item['unit'].toString() +
            ' ' +
            item['name'].toString() +
            '\n';
      }

      for (var item in responseJson['analyzedInstructions'][0]['steps']) {
        instructions +=
            item['number'].toString() + '. ' + item['step'].toString() + '\n';
      }

      responseJson['ingredients'] = ingredients;
      responseJson['instructions'] = instructions;

      return responseJson;
    }
    return {};
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double width = size.width;
    final double height = size.height;
    var _controller = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width / 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const ImageText(text: "My favorite thing to do at home is"),
                  const ImageText(text: "COOK          "),
                  ElevatedButton(
                    onPressed: () async {
                      Map result = await _getRandomRecipe();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => RecipeDetail(data: result),
                        ),
                      );
                    },
                    child: const Text("randomize!"),
                  ),
                ],
              ),
              width: width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.25),
                    BlendMode.srcOver,
                  ),
                  fit: BoxFit.cover,
                  image: const AssetImage(
                    "assets/images/food.jpg",
                  ),
                ),
              ),
            ),
          ),
          TextField(
            controller: _controller,
            decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                suffixIcon: IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () {
                    _controller.clear();
                    FocusScope.of(context).unfocus();
                    setState(() {
                      _recipes = [];
                      _resultFlex = 0;
                      _loading = "";
                    });
                  },
                ),
                hintText: 'Search recipes...',
                border: InputBorder.none),
            onTap: () {
              setState(() {
                _recipes = [];
                _resultFlex = 0;
                _loading = "";
              });
            },
            onSubmitted: (value) {
              if (value != "") {
                setState(() {
                  _resultFlex = 2;
                  _loading = "Loading...";
                });
                _getRecipe(value);
              }
            },
          ),
          _recipes.isEmpty
              ? Expanded(
                  flex: _resultFlex,
                  child: Center(
                    child: Text(_loading),
                  ),
                )
              : Expanded(
                  flex: _resultFlex,
                  child: Container(
                    padding: EdgeInsets.only(
                      bottom: height / 20,
                    ),
                    child: ListView.builder(
                      itemCount: _recipes.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () async {
                          Map result =
                              await _getRecipeDetail(_recipes[index].id);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => RecipeDetail(data: result),
                            ),
                          );
                        },
                        child: Container(
                          child: Container(
                            alignment: Alignment.bottomLeft,
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              _recipes[index].title,
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          width: width / 1.6,
                          margin: const EdgeInsets.only(
                            left: 20.0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              colorFilter: ColorFilter.mode(
                                  Colors.black.withOpacity(0.25),
                                  BlendMode.srcOver),
                              image: NetworkImage(_recipes[index].image),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}

class ImageText extends StatelessWidget {
  const ImageText({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
      ),
      fit: BoxFit.fitWidth,
    );
  }
}
