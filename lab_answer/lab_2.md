# Lab 2
## Adietya Christian - 2006595860

1. Apakah perbedaan antara JSON dan XML?

| XML                                                                                                | JSON                                                                                                                           |
|----------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| Typeless                                                                                           | Mempunyai tipe objek                                                                                                           |
| Sintaks menyerupai HTML, menggunakan tag pembuka dan penutup.                                      | Sintaks menyerupai object di Javascript, menggunakan kurung kurawal dan titik dua sebagai pemisah antara nama objek dan nilai. |
| mendukung namespaces                                                                               | tidak mendukung namespaces                                                                                                     |
| Sintaks lebih panjang                                                                              | Sintaks lebih sederhana dan lebih mudah dipahami                                                                               |
| mendukung comment                                                                                  | tidak mendukung comment                                                                                                        |
| Ukuran dokumen lebih besar karena menggunakan struktur data tree                                   | Ukuran dokumen lebih kecil dan compact dibanding XML                                                                           |
| merupakan markup language                                                                          | merupakan data format                                                                                                          |
| Mendukung banyak tipe data kompleks seperti grafik, gambar, dan beberapa tipe non-primitif lainnya | Hanya mendukung string, angka, boolean, dan object yang berisi tipe primitif saja                                              |
| Pemrosesan data pada XML lebih lambat karena ukuran datanya yang lebih besar dibanding JSON        | Pemrosesan data pada JSON lebih cepat karena didukung javascript engine dan ukuran datanya lebih kecil dibanding XML           |

2. Apakah perbedaan antara HTML dan XML?

Perbedaan utaman antara HTML dan XML adalah HTML digunakan untuk menampilkan informasi, sedangkan XML digunakan untuk menyalurkan data dari program satu dengan program lain.

Referensi:
https://www.guru99.com/json-vs-xml-difference.html