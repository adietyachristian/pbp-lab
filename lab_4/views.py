from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)

    if form.is_valid():
        # save the form data to model
        form.save()
        return HttpResponseRedirect("/lab-4")

    response = {'form': form}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
