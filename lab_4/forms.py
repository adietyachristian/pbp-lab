from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ["to","sender","title","message"]
    input_attrs = [{
		'type' : 'text',
		'placeholder' : 'Nama Penerima',
        'class' : 'input-container'
	}, {
		'type' : 'text',
		'placeholder' : 'Nama Pengirim',
        'class' : 'input-container'
	}, {
        'type' : 'text',
        'placeholder' : 'Judul',
        'class' : 'input-container'
    }, {
        'type' : 'text',
        'placeholder' : 'Tulis pesan di sini ...',
        'class' : 'input-container'
    }
    ]

    to = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs[0]))
    sender = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs[1]))
    title = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs[2]))
    message = forms.CharField(label='', required=True, widget=forms.Textarea(input_attrs[3]))
