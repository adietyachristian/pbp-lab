import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculatorPage extends StatefulWidget {
  const CalculatorPage({Key? key}) : super(key: key);

  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  String display = "";
  String result = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              color: Colors.white,
              alignment: Alignment.bottomRight,
              padding: EdgeInsets.all(20.0),
              child: Text(
                display,
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 40.0,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: elementButton(text: '1'),
                      ),
                      Expanded(
                        child: elementButton(text: '2'),
                      ),
                      Expanded(
                        child: elementButton(text: '3'),
                      ),
                      Expanded(
                        child: elementButton(text: '-'),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: elementButton(text: '4'),
                      ),
                      Expanded(
                        child: elementButton(text: '5'),
                      ),
                      Expanded(
                        child: elementButton(text: '6'),
                      ),
                      Expanded(
                        child: elementButton(text: '+'),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: elementButton(text: '7'),
                      ),
                      Expanded(
                        child: elementButton(text: '8'),
                      ),
                      Expanded(
                        child: elementButton(text: '9'),
                      ),
                      Expanded(
                        child: elementButton(text: '*'),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: elementButton(text: 'C'),
                      ),
                      Expanded(
                        child: elementButton(text: '0'),
                      ),
                      Expanded(
                        child: elementButton(text: '/'),
                      ),
                      Expanded(
                        child: elementButton(text: '='),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String calculate(String text) {
    try {
      Parser p = Parser();
      Expression exp = p.parse(text);
      ContextModel cm = ContextModel();
      result = exp.evaluate(EvaluationType.REAL, cm).toString();
      if (result.substring(result.length - 1) == "0") {
        result = result.substring(0, result.length - 2);
      }
      if (result.length > 13) {
        result = result.substring(0, 14);
      }
      return result;
    } catch (e) {
      return "Error";
    }
  }

  GestureDetector elementButton({required String text}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (text == "C") {
            display = "";
          } else if (text == "=") {
            display = calculate(display);
          } else if (display == "Error") {
            display = "";
            display += text;
          } else if (display == result) {
            display = "";
            display += text;
            result = "";
          } else {
            display += text;
          }
        });
      },
      child: Container(
        color: Colors.black87,
        child: Center(
          child: Text(
            text,
            style: const TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
