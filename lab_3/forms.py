from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ["name","npm","dob"]
    input_attrs = [{
		'type' : 'text',
		'placeholder' : 'Nama Teman Kamu'
	}, {
		'type' : 'text',
		'placeholder' : 'NPM Teman Kamu'
	}, {
        'type' : 'date',
    }
    ]

    name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs[0]))
    npm = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs[1]))
    dob = forms.DateField(label='', required=True, widget=forms.DateInput(input_attrs[2]))
