# Generated by Django 3.2.7 on 2021-09-16 17:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0003_alter_friend_dob'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='npm',
            field=models.IntegerField(max_length=10),
        ),
    ]
